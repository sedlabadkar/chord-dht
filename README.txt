Simulates a distributed hash table implementing the Chord algorithm for node management.

To Run : scalac project3 <numNodes> <numRequests>

<numNodes>: Number of nodes to simulate along the DHT
<numRequests>: Number of request run by each node before the program terminates
